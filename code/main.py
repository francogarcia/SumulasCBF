import copy
import json
import os
import pandas
import re


PLAYERS = "players"
TURNS = "cards_per_turn"
RED_CARDS = "red_cards"
YELLOW_CARDS = "yellow_cards"
TOTAL_YELLOW_CARDS = "total_yellow_cards"
TOTAL_RED_CARDS = "total_red_cards"

LONG_TEAM_NAME = {
    "clube atlético paranaense/pr": "atlético/pr",
    "clube atletico paranaense/pr": "atlético/pr",
    "atletico/pr" : "atlético/pr",

    "clube atlético mineiro/mg": "atlético/mg",
    "clube atletico mineiro/mg": "atlético/mg",

    "esporte clube bahia/ba": "bahia/ba",
    "bahia/ba": "bahia/ba",

    "botafogo de futebol e regatas/rj": "botafogo/rj",

    "associação chapecoense de futebol/sc": "chapecoense/sc",
    "associacao chapecoense de futebol/sc": "chapecoense/sc",

    "sport club corinthians paulista/sp": "corinthians/sp",

    "coritiba foot-ball club/pr": "coritiba/pr",
    "coritiba/pr": "coritiba/pr",

    "criciúma esporte clube/sc": "criciúma/sc",
    "criciuma esporte clube/sc": "criciúma/sc",
    "criciuma/sc": "criciúma/sc",

    "cruzeiro esporte clube/mg": "cruzeiro/mg",

    "figueirense futebol clube/sc": "figueirense/sc",

    "clube de regatas do flamengo/rj": "flamengo/rj",

    "fluminense football club/rj": "fluminense/rj",

    "goiás esporte clube/go": "goiás/go",
    "goias esporte clube/go": "goiás/go",
    "goias/go": "goiás/go",

    "grêmio foot-ball porto alegrense/rs": "grêmio/rs",
    "gremio foot-ball porto alegrense/rs": "grêmio/rs",
    "gremio/rs": "grêmio/rs",

    "clube náutico capibaribe/pe": "náutico/pe",
    "clube nautico capibaribe/pe": "náutico/pe",
    "nautico/pe": "náutico/pe",

    "sport club internacional/rs": "internacional/rs",

    "santos futebol clube/sp": "santos/sp",

    "sociedade esportiva palmeiras/sp": "palmeiras/sp",

    "sport club do recife/pe": "sport/pe",

    "são paulo futebol clube/sp": "são paulo/sp",
    "sao paulo futebol clube/sp": "são paulo/sp",
    "sao paulo/sp": "são paulo/sp",

    "vasco da gama/rj": "vasco/rj",

    "esporte clube vitória/ba": "vitória/ba",
    "vitória/ba": "vitória/ba"
}


def team_long_to_popular_name(teamName):
    teamName = teamName.lower()
    if (teamName in LONG_TEAM_NAME):
        return LONG_TEAM_NAME[teamName]

    return teamName


def extract_booked_players(matchSummary, patternRegex, cardKey, teams, turnNumber, fileName, fileHeader):
    regex_find_player_name_and_team = re.compile(patternRegex)
    regex_results = regex_find_player_name_and_team.findall(matchSummary)

    results = fileHeader + "\n\n"
    for (playerName, playerTeam) in regex_results:
        results +=  "Jogador:" + playerName + "\n"
        results +=  "Time:" + playerTeam + "\n\n"

        playerName = playerName.lower()
        playerTeam = team_long_to_popular_name(playerTeam)
        if (not playerTeam in teams):
            teams[playerTeam] = {PLAYERS : {},
                                 TURNS : {}}

        team = teams[playerTeam]

        teamPlayers = team[PLAYERS]
        if (not playerName in teamPlayers):
            teamPlayers[playerName] = {YELLOW_CARDS : 0, RED_CARDS : 0}
        teamPlayers[playerName][cardKey] += 1

        teamCardsTurn = team[TURNS]
        if (not turnNumber in teamCardsTurn):
            teamCardsTurn[turnNumber] = {YELLOW_CARDS : 0, RED_CARDS : 0, TOTAL_YELLOW_CARDS : 0, TOTAL_RED_CARDS : 0}
        teamCardsTurn[turnNumber][cardKey] += 1

        # print(results)

    with open(fileName, "w", encoding="utf8") as textFile:
        textFile.write(results)

    return


def main():
    for year in [2013, 2014, 2015, 2016]:
        # Stores carded players for every team.
        teams = {}

        # NOTE Run this script using the README.org file as working directory.
        filePathBase = "code/sumulas/" + str(year) + "/"
        filePathPDF = filePathBase + "pdf/"
        filePathTXT = filePathBase + "txt/"
        filePathYellowCards = filePathBase + "yellow_cards/"
        filePathRedCards = filePathBase + "red_cards/"
        matchNumber = 0
        # Lambda: natural sorting from <https://stackoverflow.com/a/16090640>
        for filePath in sorted(os.listdir(filePathPDF), key=lambda s: [int(t) if t.isdigit() else t.lower() for t in re.split('(\d+)', s)]):
            if (not filePath.endswith(".pdf")):
                continue

            fileName, fileExtension = os.path.splitext(filePath)
            filePDF = filePathPDF + fileName + ".pdf"
            fileText = filePathTXT + fileName + ".txt"

            # Convert PDF to TXT, if needed.
            if (not os.path.isfile(fileText)):
                subprocess.call(["ebook-convert", filePDF, fileText])

            # Extract the content regarding booking in each match summary.
            bufferYellowCards = ""
            bufferRedCards = ""
            with open(fileText, encoding="utf8") as inputFile: #, open(fileName + "_yellow_cards.txt", "w", encoding="utf8") as outputFileYellowCard, open(fileName + "_red_cards.txt", "w", encoding="utf8") as outputFileRedCard:
                bCopyContentYellowCard = False
                bCopyContentRedCard = False
                for line in inputFile:
                    if ("Cartões Amarelos" in line.strip()):
                        bCopyContentYellowCard = True
                        bCopyContentRedCard = False
                    elif ("Cartões Vermelhos" in line.strip()):
                        bCopyContentYellowCard = False
                        bCopyContentRedCard = True
                    elif ("Ocorrências / Observações" in line.strip()):
                        bCopyContentYellowCard = False
                        bCopyContentRedCard = False
                    elif (bCopyContentYellowCard):
                        # outputFileYellowCard.write(line)
                        bufferYellowCards += line
                    elif (bCopyContentRedCard):
                        # outputFileRedCard.write(line)
                        bufferRedCards += line

            turnNumber = (matchNumber // 10) + 1
            matchNumber += 1

            # Collect booked players in each match summary.
            # First for yellow cards...
            extract_booked_players(bufferYellowCards,
                                   r'[1-2]T\n\n\d+\n\n(?P<jogador>(?:[\w ]+?))(?:\n\n| | - )(?P<time>(?:Santa |São |Ponte |Vasco da )?[\w]+\/[A-Z][A-Z])\n',
                                   YELLOW_CARDS,
                                   teams,
                                   turnNumber,
                                   filePathYellowCards + fileName + "_players_with_yellow_cards.txt",
                                   "Jogadores que receberam cartão amarelo:")
            # Then for red cards.
            extract_booked_players(bufferRedCards,
                                   # r'[1-2]T\n\n\d+\n\n(?P<jogador>(?:[\w ]+?))(?:\n\n| - )(?P<time>(?:Santa |São |Ponte |Vasco da )?[\wáàãâéèẽêíìĩîóòõôúùũûç\- ]+\/[A-Z][A-Z])(?:\n|[\s]+Cartão Vermelho Direto|[\s]+2(?:o|º) Cartão Amarelo)',
                                   r'[1-2]T\n\n\d+\n\n(?P<jogador>(?:[\w ]+?))(?:\n\n| - )(?P<time>(?:Santa |São |Ponte |Vasco da )?[\wáàãâéèẽêíìĩîóòõôúùũûç\- ]+\/[A-Z][A-Z])(?:\n|[\s])',
                                   RED_CARDS,
                                   teams,
                                   turnNumber,
                                   filePathRedCards + fileName + "_players_with_red_cards.txt",
                                   "Jogadores que receberam cartão vermelho:")

        # Sums all cards per teams.
        for team in teams:
            totalYellowCards = 0
            totalRedCards = 0
            teamData = teams[team]
            players = teamData[PLAYERS]
            for player in players:
                playerData = players[player]
                totalYellowCards += playerData[YELLOW_CARDS]
                totalRedCards += playerData[RED_CARDS]
            teamData[TOTAL_YELLOW_CARDS] = totalYellowCards
            teamData[TOTAL_RED_CARDS] = totalRedCards
            # Cards per turn.
            turns = teamData[TURNS]
            # Subtotal per turn now.
            totalYellowCards = 0
            totalRedCards = 0
            for turn in turns:
                turnData = turns[turn]
                totalYellowCards += turnData[YELLOW_CARDS]
                totalRedCards += turnData[RED_CARDS]
                turnData[TOTAL_YELLOW_CARDS] = totalYellowCards
                turnData[TOTAL_RED_CARDS] = totalRedCards

        # Serialize data to JSON.
        # jsonData = json.dumps(teams) # To string.
        outputTeamsFileName = filePathBase + "teams" + str(year) + ".json"
        with open(outputTeamsFileName, "w", encoding="utf8") as outputFile:
            json.dump(teams, outputFile, indent=4, sort_keys=True, ensure_ascii=False)

        print("Data dumped onto " + outputTeamsFileName)

        # Save data in CSV.
        outputTeamsFileName = filePathBase + "teams" + str(year) + ".csv"
        with open(outputTeamsFileName, "w", encoding="utf8") as outputFile:
            outputFile.write("Time," + "Nome do Jogador," +  "Cartões Amarelos," + "Cartões Vermelhos\n")
            for team in teams:
                teamData = teams[team]
                players = teamData[PLAYERS]

                for player in players:
                    playerData = players[player]
                    outputFile.write(team + "," + player + "," + str(playerData[YELLOW_CARDS]) + "," + str(playerData[RED_CARDS]) + "\n")

        print("Data dumped onto " + outputTeamsFileName)

        # Cards per turn in CSV.
        outputTeamsFileName = filePathBase + "teams" + str(year) + "_cards_turn.csv"
        with open(outputTeamsFileName, "w", encoding="utf8") as outputFile:
            outputFile.write("Time," + "Rodada," +  "Cartões Amarelos," + "Cartões Vermelhos," + "Total de Cartões Amarelos," + "Total de Cartões Vermelhos" + "\n")
            for team in teams:
                teamData = teams[team]
                teamCardsTurn = teamData[TURNS]

                for turn in teamCardsTurn:
                    cardsTurn = teamCardsTurn[turn]
                    outputFile.write(team + "," + str(turn) + "," + str(cardsTurn[YELLOW_CARDS]) + "," + str(cardsTurn[RED_CARDS]) + "," + str(cardsTurn[TOTAL_YELLOW_CARDS]) + "," + str(cardsTurn[TOTAL_RED_CARDS]) + "\n")

        print("Data dumped onto " + outputTeamsFileName)

        # Save data to Excel.

        # This is using Pandas. It could also be used to generate the CSV file (dataFram.to_csv()).

        # http://pbpython.com/pandas-list-dict.html

        # 1) First way.
        # outputTeamsFileName = filePathBase + "teams" + str(year) + ".xlsx"
        # cardsPerTeam = []
        # for team in teams:
        #     teamData = teams[team]
        #     players = teamData[PLAYERS]

        #     for player in players:
        #         playerData = players[player]
        #         cardsPerTeam.append(copy.deepcopy({
        #             "Time": team,
        #             "Nome do Jogador": player,
        #             "Cartões Amarelos": playerData[YELLOW_CARDS],
        #             "Cartões Vermelhos": playerData[RED_CARDS]
        #         }))

        # dataFrame = pandas.DataFrame(cardsPerTeam)# .T # Transpose
        # outputTeamsFileName = filePathBase + "teams" + str(year) + ".xlsx"
        # dataFrame.to_excel(outputTeamsFileName)

        # 2) Second way.
        outputTeamsFileName = filePathBase + "teams" + str(year) + ".xlsx"
        cardsPerTeam = {
            "Time": [],
            "Nome do Jogador": [],
            "Cartões Amarelos": [],
            "Cartões Vermelhos": []
        }
        for team in teams:
            teamData = teams[team]
            players = teamData[PLAYERS]

            for player in players:
                playerData = players[player]
                cardsPerTeam["Time"].append(team)
                cardsPerTeam["Nome do Jogador"].append(player)
                cardsPerTeam["Cartões Amarelos"].append(playerData[YELLOW_CARDS])
                cardsPerTeam["Cartões Vermelhos"].append(playerData[RED_CARDS])

        dataFrame = pandas.DataFrame.from_dict(cardsPerTeam)
        dataFrame = dataFrame[["Time", "Nome do Jogador", "Cartões Amarelos", "Cartões Vermelhos"]]
        dataFrame.to_excel(outputTeamsFileName, index=False)

        print("Data dumped onto " + outputTeamsFileName)

        outputTeamsFileName = filePathBase + "teams" + str(year) + "_cards_turn.xlsx"
        cardsPerTeam = {
            "Time": [],
            "Rodada": [],
            "Cartões Amarelos": [],
            "Cartões Vermelhos": [],
            "Total de Cartões Amarelos": [],
            "Total de Cartões Vermelhos": []
        }
        for team in teams:
            teamData = teams[team]
            teamCardsTurn = teamData[TURNS]

            for turn in teamCardsTurn:
                cardsTurn = teamCardsTurn[turn]
                cardsPerTeam["Time"].append(team)
                cardsPerTeam["Rodada"].append(turn)
                cardsPerTeam["Cartões Amarelos"].append(cardsTurn[YELLOW_CARDS])
                cardsPerTeam["Cartões Vermelhos"].append(cardsTurn[RED_CARDS])
                cardsPerTeam["Total de Cartões Amarelos"].append(cardsTurn[TOTAL_YELLOW_CARDS])
                cardsPerTeam["Total de Cartões Vermelhos"].append(cardsTurn[TOTAL_RED_CARDS])

        dataFrame = pandas.DataFrame.from_dict(cardsPerTeam)
        dataFrame = dataFrame[["Time", "Rodada", "Cartões Amarelos", "Cartões Vermelhos", "Total de Cartões Amarelos", "Total de Cartões Vermelhos"]]
        dataFrame.to_excel(outputTeamsFileName, index=False)

        print("Data dumped onto " + outputTeamsFileName)


if __name__ == "__main__":
    main()
